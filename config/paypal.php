<?php
	return[


'sendbox.client_id' =>env('PAYPAL_SENDBOX_CLIENT_ID'),

'sendbox.secret' =>env('PAYPAL_SENDBOX_SECRET'),

'settings'=>[
'mode'=>env('PAYPAL_MODE','sendbox'),
'http.ConnectionTimeOut'=>3000,
'log.LongEndabled'=>true,
'log.FileName'=>storage_path() .'/log/paypal.log',
'log.LogLevel'=>'DEBUG'


]
	];