<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=> ['web' => 'auth']],function (){


Route::get('newsa/{id}', 'HomeController@orderByDrop');
Route::get('/live_search/action', 'HomeController@action')->name('live_search.action');

	Route::resource('product','product');
Route::resource('category','cate');
	Route::resource('events','eventsConroller');

	Route::get('home',function(){
if(Auth::user()->admin == 0){
	return view('home');
}else{
	$user=\App\User::all();
return 	 view('admin.index', compact('user'));

}
	});
		});

// fornt 
Route::get('/', 'FrontController@index');
Route::post('sreachpage', 'FrontController@sareac');
Route::get('mail', 'FrontController@maila');
Route::get('event', 'FrontController@events');
Route::get('About', 'FrontController@About');
Route::get('products', 'FrontController@product');
Route::get('cate/{id}', 'FrontController@cate');
Route::get('Detail/{id}', 'FrontController@Detail');
Route::get('read/{id}/{por}', 'FrontController@read');

Route::get('add/{id}', 'ca@addcart');
Route::get('cart', 'ca@index');
Route::get('remove/{id}','ca@destroy');
Route::PUT('/Cart/update/{id}','ca@update');
//paym
Route::post('pay', 'PaymentController@paywithpaypal');
Route::get('stauts','PaymentController@stauts');
Route::get('Canceled','PaymentController@Canceled');



