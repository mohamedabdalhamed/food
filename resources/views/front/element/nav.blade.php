<!-- header -->
	<div class="agileits_header">
		<div class="w3l_offers">
			<a href="{{url('/')}}">Today's special Offers !</a>
		</div>
		<div class="w3l_search">
			<form action="{{url('sreachpage')}}" method="post" id="A">
					{{csrf_field()}}

				<input type="text" name="Product" value="Search a product..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
			<input type="submit" name="" id="add" value=" ">
				
			</form>
		</div>
		<div class="product_list_header" style="margin-top: 10px;">  
			<a href="{{url('cart')}}" style="    color: #fff;
    font-size: 14px;
    outline: none;
    text-transform: capitalize;
    padding: .5em 2.5em .5em 1em;
    border: 1px solid #84c639;
    margin: .35em 0 0;

    background: url(images/cart.png) no-repeat 116px 9px;">View your cart</a>
			
		</div>
		<div class="w3l_header_right">
			<ul>
				<li class="dropdown profile_details_drop">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i><span class="caret"></span></a>
					<div class="mega-dropdown-menu">
						<div class="w3ls_vegetables">
							<ul class="dropdown-menu drp-mnu">
								@guest
					<li><a href="{{route('login')}}">Login</a></li> 
								<li><a href="{{route('login')}}">Sign Up</a></li>

					@else


													<li><a href="{{url('home')}}">profile</a></li> 

								@endguest
							</ul>
						</div>                  
					</div>	
				</li>
			</ul>
		</div>
		<div class="w3l_header_right1">
			<h2><a href="{{url('mail')}}">Contact Us</a></h2>
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- script-for sticky-nav -->
	<script>
	$(document).ready(function() {
		 var navoffeset=$(".agileits_header").offset().top;
		 $(window).scroll(function(){
			var scrollpos=$(window).scrollTop(); 
			if(scrollpos >=navoffeset){
				$(".agileits_header").addClass("fixed");
			}else{
				$(".agileits_header").removeClass("fixed");
			}
		 });
		 
	});
	</script>
<!-- //script-for sticky-nav -->
	<div class="logo_products">
		<div class="container">
			<div class="w3ls_logo_products_left">
				<h1><a href="{{url('/')}}"><span>FoodsEarth</span> Store</a></h1>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="special_items">
					<li><a href="{{url('event')}}">Events</a><i>/</i></li>
					<li><a href="{{url('About')}}">About Us</a><i>/</i></li>
					<li><a href="{{url('products')}}">Best Deals</a><i>/</i></li>
					<li><a href="{{url('services')}}">Services</a></li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>+201229366164</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a >mohamedadbhiamed2@gmail.com</a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>


<script type="text/javascript">
      
$(document).on('click','#add',function() {
    var form=$('#A').serialize();
    var url=$('#A').attr('action');
    // alert(form)
   $.ajax({
    url:url,
    dataType:'json',
    data:form,
    type:'post',
    success:function(data){
// $('#A').hide();
//  $(".ale").show();
if(data.length == 0){
$('.flexslider').hide();
$('#sre').html("NO product");
}else{
	$('.flexslider').hide();
var a='';
  a+="<div class='container' >";
    a+="<div class='row' >";

           for(var i=0;i<data.length;i++){
	
    a+="<div class='col-md-3' style='margin:10px'>";

a+="<div class='col-md-6'> ";
a+="<img src='image/"+data[i].image+"' style='max-width: 200px; max-height: 150px'  >";  
     a+="</div>";
a+="<div class='col-md-6'> ";

a+="<h3 class='text-center'>";
a+=data[i].pro_name;
a+="</h3>";
a+="<h5 class='text-center'>";
a+=data[i].pro_price;
a+="</h5>";
     a+="</div>";

    a+="</div>";
   }
     a+="</div>";
      a+="</div>";

                                           $("#sre").html(a);


}
    }
   });
    // alert(url);    
return false;
});
</script>