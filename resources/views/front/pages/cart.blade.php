@extends('front.layout.default')
@section('title','Cart')
@section('content')	
<script>
        $(document).ready(function(){
            <?php for($i=1;$i<20;$i++){?>
$('#upCart<?php echo $i;?>').on('change keyup', function(){
                var newqty = $('#upCart<?php echo $i;?>').val();
                var rowId = $('#rowId<?php echo $i;?>').val();
                var proId = $('#proId<?php echo $i;?>').val();
                if(newqty <=0){ alert('enter only valid qty') }
                else {
                    $.ajax({
                        type: 'get',
                        responseType: 'html',
                        url: '<?php echo url('/cart/update');?>/'+proId,
                        response: "qty=" + newqty + "& rowId=" + rowId + "& proId=" + proId,
                        success: function (response) {
                            console.log(response);
                            $('#updateDiv').html(response);
                        }
                    });
                }
            });
            <?php } ?>
        });
    </script>
    		<div id="sre"> </div>

      <?php if ($a->isEmpty()) { ?>
    <br>
    <br>
    <br>

    <section id="cart_items">
        <div class="container">

            <div align="center">
<div class="alert-danger" style="height: auto 200pc; ">
             <h1>empty cart shop </h1> </div>
    </div>
        </div>
    </section> <!--/#cart_items-->
    <?php } else { ?>
<div class="container">
	<div class="row">
<table class="table table-responvie">
	<thead>
		<tr>
		 <th>id</th>
		 <th>name</th>
		 <th>price</th>
		 <th>image</th>
	     <td>qinty</td>
	    </tr>
    </thead>
 
  <tbody >
                        <?php $count =1;?>
  	@foreach($a as $as)
 <tr id="updateDiv">
 	 	  <td>{{$as->id}}</td>

 	  <td>{{$as->name}}</td>
 	  <td>${{$as->price}}</td>
           
 	  <td> <p><img src="{{url('image',$as->options->img)}}" class="img-responsive"   style="max-width: 200px; max-height: 150px"  ></p></td>
 	         <td class="cart_quantity">
                                    <form action="{{url('Cart/update',$as->rowId)}}" method="post" role="form">

                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="proID" value="{{$as->id}}"/>
                                        <input type="number" size="2" value="{{$as->qty}}" name="qty" id="upCart<?php echo $count;?>"
                                               autocomplete="off" style="text-align:center; max-width:50px; "  MIN="1" MAX="1000">
                                        <input type="submit" class="btn btn-primary" value="Update" styel="margin:5px">
                                    </form>

                                    <!--</div>-->
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$as->subtotal}}</p>
                                </td>
                                <td class="cart_delete">
                                    <button class="btn btn-primary">
                                        <a class="cart_quantity_delete"  href="{{url('remove',$as->rowId)}}" ><i class="fa fa-times"></i></a>
                                    </button>
                                </td>
                            <?php $count++;?>
  
 @endforeach
  </tbody>
</table>
	</div>
</div>    <?php } ?>
>
	@endsection