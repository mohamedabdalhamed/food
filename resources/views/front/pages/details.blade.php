@extends('front.layout.default')
@section('title','Details')
@section('content')
<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{url('/')}}">Home</a><span>|</span></li>
				<li>Details Page</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->
<!-- banner -->

	<div class="banner">

		<div class="w3l_banner_nav_left">
			<nav class="navbar nav_bottom">
			 <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header nav_2">
				  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
			   </div> 
			   <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav nav_1">
							@foreach($category as $na)
						<li><a href="{{url('cate',$na->id)}}">{{$na->name}}</a></li>
	@endforeach
									</ul>
						
				 </div><!-- /.navbar-collapse -->
			</nav>
		</div>
						<div id="sre"> </div>

		<div class="w3l_banner_nav_right">
			@foreach($pro as $a)
			<div class="w3l_banner_nav_right_banner3">

				<h3>Best Deals For New Products<span class="blink_me"></span></h3>
			</div>
			<div class="agileinfo_single">
				<h5>{{$a->pro_name}}</h5>
				<div class="col-md-4 agileinfo_single_left">
					<img id="example" src="{{url('image',$a->image)}}" alt=" "  style="max-width: 200px; max-height: 150px" class="img-responsive" />
				</div>
				<div class="col-md-8 agileinfo_single_right">
					@guest
	<div class="rating1">
						<span class="starRating" id="as" >
							<input id="rating5" type="radio" name="rating" value="5" 	<?php if($s <= 5){
						echo "checked";
					} ?> readonly >
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4" 	<?php if($s <= 4){
						echo "checked";
					} ?> readonly >
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" 	<?php if($s <= 3){
						echo "checked";
					} ?> readonly >
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2" 	<?php if($s <= 2){
						echo "checked";
					} ?> readonly >
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1"
					<?php if($s <= 1){
						echo "checked";
					} ?> readonly >
							<label for="rating1">1</label>
						</span>
					</div>					@else
												<input type="hidden" name="" id="sss" value="{{$a->id}}">

					<div class="rating1">
						<span class="starRating" id="as" >
							<input id="rating5" type="radio" name="rating" value="5" 	<?php if($s <= 5){
						echo "checked";
					} ?> >
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4" 	<?php if($s <= 4){
						echo "checked";
					} ?> >
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" 	<?php if($s <= 3){
						echo "checked";
					} ?> >
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2" 	<?php if($s <= 2){
						echo "checked";
					} ?> >
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1"
					<?php if($s <= 1){
						echo "checked";
					} ?> >
							<label for="rating1">1</label>
						</span>
					</div>
					@endguest

					<div class="w3agile_description">
						<h4>Description :</h4>
						<p>{{$a->pro_info}}</p>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
											<h4>{{$a->spl_price}}<span>{{$a->pro_price}}</span></h4>
						</div>

						<div class="snipcart-details agileinfo_single_right_details">
						<form action="{{url('add')}}" method="GET" id="AA">
												<fieldset>
													<input type="hidden" name="pro_id" id="pro_id" value="{{$a->id}}" />
												
													<input type="submit" id="addd" name="submit" value="Add to cart" class="button" />
												</fieldset>
													
											</form>
											<div>
												
												<form method="post" action="{{url('pay')}}">
                                					{{csrf_field()}}
  
                                                 <input type="hidden" name="price" value="{{$a->spl_price}}">

													<input type="submit" name="" value="Buy Now" class="btn btn-info">
												</form>
											</div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
					@endforeach

		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->
<!-- brands -->
<!-- //brands -->
          <script>
    $(document).ready(function(){

    $('#as').change(function(){
          var teach_id = $('#as input:checked').attr('value');
                    var as = $('#sss').attr('value');

            
      $.ajax({
    type:'GET',
        url:"{{url('read')}}/"+  teach_id +"/"+as,
    dataType:'json',

    success:function(data){
    	console.log(data.length);


    },
   });  
alert(teach_id);
            
    });
return false;
      });



 
$(document).on('click','#addd',function() {

    var form=$('#pro_id').val();
    // alert(form)
   $.ajax({
    url:"{{url('add')}}/"+form,
    type:'get',
    success:function(data){
$("#addd").hide();


 }
   });
    // alert(url);   


return false;


});


    </script>
	@Endsection
