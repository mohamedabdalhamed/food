@extends('admin.master')
@section('title','Add product')
@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 text-center">
        <h1>Add product</h1>
<div class="col-md-6">
	<div class="panel-body">
		<form action="{{route('product.store')}}" method="post" role="form"  enctype="multipart/form-data">
			<legend>Add product</legend>
			{{csrf_field()}}
			<div class="form-group {{$errors->has('pro_name')?'has-error':''}}">
				<label for="pro_name">product Name</label>
				<input type="text" name="pro_name" id="pro_name"  class="form-control" >
				<span class="text-danger">{{$errors->first('pro_name')}}</span>
			</div>
				 <div class="form-group {{$errors->has('pro_code')?'has-error':''}}">
				<label for="pro_code">code</label>
				<input type="text" name="pro_code" id="pro_code"  class="form-control" >
				<span class="text-danger">{{$errors->first('pro_code')}}</span>
			     </div>
				   <div class="form-group {{$errors->has('pro_price')?'has-error':''}}">
				<label for="pro_price">price</label>
				<input type="text" name="pro_price" id="pro_price"  class="form-control" >
				<span class="text-danger">{{$errors->first('pro_price')}}</span>
			       </div>

			          <div class="form-group {{$errors->has('stook')?'has-error':''}}">
				<label for="stook">stook</label>
				<input type="text" name="stook" id="stook"  class="form-control" >
				<span class="text-danger">{{$errors->first('stook')}}</span>
			       </div>



				    <div class="form-group {{$errors->has('pro_info')?'has-error':''}}">
				<label for="pro_info">Description</label>
				<textarea  name="pro_info" id="pro_info"  class="form-control"  rows="5"></textarea> 
				<span class="text-danger">{{$errors->first('pro_info')}}</span>
			        </div>


    <div class="form-group {{$errors->has('categories_id')?'has-error':''}}">
				<label for="categories_id">Catgeroy</label>

				<select name="categories_id" id="categories_id">
					@foreach($cat as  $id=>$cats)
					<option value="{{$id}}">{{$cats}}</option>
					@endforeach
				</select>

				<span class="text-danger">{{$errors->first('categories_id')}}</span>
			        </div>
 

    <div class="form-group {{$errors->has('offier')?'has-error':''}}">
				<label for="offier">Catgeroy</label>

				<select name="offier" id="offier">
			
					<option value="0">no offier</option>
					<option value="1">offier</option>

				</select>

				<span class="text-danger">{{$errors->first('offier')}}</span>
			        </div>
 


			           <div class="form-group {{$errors->has('image')?'has-error':''}}">
				<label for="image">image</label>
				<input type="file" name="image" id="image"  class="form-control" >
				<span class="text-danger">{{$errors->first('image')}}</span>
			           </div> 

                         <div class="form-group {{$errors->has('spl_price')?'has-error':''}}">
				<label for="spl_price">sale price</label>
				<input type="text" name="spl_price" id="spl_price"  class="form-control" >
				<span class="text-danger">{{$errors->first('spl_price')}}</span>
			             </div>
<button type="submiy" class="btn btn-info">Add product</button>
		</form>	
	</div>
</div>
        
        </main>
@Endsection