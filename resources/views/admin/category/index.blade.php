@extends('admin.master')
@section('title','List categorys')
@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <h1>List category</h1>
        <a href="{{route('category.create')}}" class="btn btn-info">Add category</a>
<table class="table">
	<tr>
		<thead>
		<td>id</td>
		<td>name</td>
	<td>control</td>
</thead>
<tbody>
	</tr>
@foreach($categorys  as $category)
	<tr>
<td>{{$category->id}}</td>
<td>{{$category->name}}</td>
<td>	
	<form action="{{route('category.destroy',$category->id)}}" method="post">
	{{csrf_field()}}
	{{method_field('DELETE')}}
	<input type="submit" class="btn btn-danger" value="DELETE"> 
    </form>
</td>

	
	</tr>
	@endforeach
</tbody>
</table>

        
        </main>
@Endsection