@extends('admin.master')
@section('title','Add categorys')
@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 text-center">
        <h1>Add categorys</h1>
<div class="col-md-6">
	<div class="panel-body">
		<form action="{{route('category.store')}}" method="post" role="form"  enctype="multipart/form-data">
			<legend>Add categorys</legend>
			{{csrf_field()}}
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				<label for="name">product Name</label>
				<input type="text" name="name" id="name"  class="form-control" >
				<span class="text-danger">{{$errors->first('name')}}</span>
			</div>
			
<button type="submit" class="btn btn-info">Add product</button>
		</form>	
	</div>
</div>
        
        </main>
@Endsection