@extends('admin.master')
@section('title','Events')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              data here
            </div>
          </div>
<div class="container">
<h1 class="text-center">Add event</h1>
<form method="post" action="{{url('events')}}" role="form"  enctype="multipart/form-data">
			{{csrf_field()}}
		<div class="row">
	
<div class="col-md-2">
	<label>Title Event</label>
</div>
<div class="col-md-10">
	
<input type="text" name="title" class="form-control" style="width: 100%;">

</div>
<br>

<br>
	
<div class="col-md-2">
	<label>Decription Event</label>
</div>
<div class="col-md-10">

<textarea name="decrption"  class="form-control" cols="140" rows="5"></textarea>
</div>
<br>
<div class="col-md-2">
	<label>DATA Event</label>
</div>
<div class="col-md-4">

<input type="date" name="data" class="form-control"><br>

</div><br>
<div class="col-md-2">
	<label>TIME Event</label>
</div>
<div class="col-md-4">

<input type="time" name="time" class="form-control"><br>
</div><br>
<div class="col-md-2">
	<label>name </label>
</div>
<div class="col-md-10">

<input type="text" name="name" class="form-control" style="width: 100%;"></div>
<br>
<div class="col-md-2">
	<label>image </label>
</div>
<div class="col-md-10">

<input type="file" name="asd" class="form-control" style="width: 100%;"></div>

</div><br>
</div><br>
<div class="text-center">
<input type="submit" class="btn btn-success " value="Add Events">
</div>
</form>
</div>
        
        </main>
@Endsection