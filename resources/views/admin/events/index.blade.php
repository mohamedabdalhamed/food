@extends('admin.master')
@section('title','Events')
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              data here
            </div>
          </div>
<div class="container">
	<a href="{{url('events/create')}}" class="btn btn-info">Add Event</a>
	<table class="table table-responve">
		<thead>
		<tr>
				<th>photo</th>
				<th>data</th>
				<th>title</th>
				<th>time</th>
				<th>name</th>
				<th>decrption</th>
				<th>control</th>
				

		</tr>
</thead>
<tbody>
	@foreach($eve as $e) 
	<tr>
		<td><img src="{{url('image/events/',$e->photo)}}" width="80" height="80"></td>
		<td>{{$e->data1}}</td>
		<td>{{$e->title}}</td>
		<td>{{$e->time}}</td>
		<td>{{$e->name}}</td>
		<td>{{$e->decrption}}</td>
		<td><form action="{{route('events.destroy',$e->id)}}" method="post">
	{{csrf_field()}}
	{{method_field('DELETE')}}
	<input type="submit" class="btn btn-danger" value="DELETE"> 
    </form>
 <a class="btn btn-success" href="{{route('events.edit',$e->id)}}">Edit</a>
</td>


	</tr>
	@endforeach
</tbody>
	</table>
</div>

        
        </main>
@Endsection