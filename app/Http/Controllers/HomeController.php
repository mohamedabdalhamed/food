<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pro;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function orderByDrop($id){

$a=pro::orderBy('id',$id)->get();
       return response($a);
}

public function action(Request $request)
    {
   
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = DB::table('pros')
         ->where('pro_name', 'like', '%'.$query.'%')
          ->orWhere('id', 'like', '%'.$query.'%')
         ->get();
         
      }
      else
      {
       $data = DB::table('pros')
         ->get();
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        

        $output .= '
        <tr>
         <td>'.$row->id.'</td>
         <td>'.$row->pro_name.'</td>
           <td><img src="image/'.$row->image.' "width="80" height="80"></td>

 <td>'.$row->pro_price.'</td>
  <td>'.$row->pro_price.'</td>

            </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

    return response($data); 
    }
}
