<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
class PaymentController extends Controller
{
    
	private $apiContext;
	private $secret;
	private $clientId;

	public function __construct(){

		if(config('paypal.settings.mode') == 'live')
		{
			

$this->secret=config('PAYPAL_live_CLIENT_ID');
$this->clientId=config('PAYPAL_live_SECRET');

		}else {
			$this->secret=config('PAYPAL_sendbox_CLIENT_ID');
$this->clientId=config('PAYPAL_sendbox_SECRET');

		}
		$this->apiContext=new ApiContext(new OAuthTokenCredential($this->clientId,$this->secret));
$this->apiContext->setConfig(config('paypal.settings'));
	}

   public function paywithpaypal(Request $request)
   {
$price=$request->price;


$payer = new Payer();
$payer->setPaymentMethod("paypal");
$item = new Item();
$item->setName($price)
    ->setCurrency('USD')
    ->setQuantity(1)
    ->setPrice($price);

$itemList = new ItemList();
$itemList->setItems([$itemList]);
$amount = new Amount();
$amount->setCurrency("USD")
    ->setTotal($price);
    $transaction = new Transaction();
$transaction->setAmount($amount)
    ->setItemList($itemList)
    ->setDescription("Payment description");

    $redirectUrls = new RedirectUrls();
$redirectUrls->setCancelUrl("http://localhost/all/stauts")->setReturnUrl("http://localhost/all/public/Canceled");
$payment = new Payment();
$payment->setIntent("sale")
    ->setPayer($payer)
    ->setRedirectUrls($redirectUrls)
    ->setTransactions(array($transaction));
    try{
$payment->create($this->apiContext);
    }catch(\PayPal\Exception\PPConnectionException $ex){
die($ex);

    }
$approvalUrl = $payment->getApprovalLink();
return redirect($approvalUrl);
}
public function stauts(){}
public function Canceled(){

	return 'pk cans';
}
}
