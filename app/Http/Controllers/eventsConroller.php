<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\event;
use Image;
use Illuminate\Support\Facades\Auth;

class eventsConroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           if(Auth::user()->admin == 1){
        $eve=event::all();
        return view('admin.events.index',compact('eve'));
        }

        else 
        {
            return view('error404');

                }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    if(Auth::user()->admin == 1){
        return view('admin.events.create');
        }

        else 
        {
            return view('error404');

                }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $a=new event();
  if($request->hasFile('asd')){
          $img=$request->asd;
    $filename=time() ."_".$img->getClientOriginalName();
    $localtion=public_path('image/events/'.$filename);
      //$img->move($localtion);
      image::make($img)->resize(860,500)->save($localtion);
      $a->photo=$filename; 
      }

     
$a->data1=$request->data;
$a->time=$request->time;
$a->title=$request->title;
$a->name=$request->name;
$a->decrption=$request->decrption;
$a->save();
return  redirect('home')->with('status','you success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          if(Auth::user()->admin == 1){
        $productdelete=event::find($id);   
        // unlink('image/events/'.$productdelete->image);

      $productdelete->delete();
      return back();;   
    } 

        else 
        {
            return view('error404');

        }
    }
}
